import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  styleUrls: ['app.component.scss'],
  template: `
    <mat-sidenav-container class="sidenav-container">
      <mat-sidenav-content>
        <mat-toolbar color="primary">
          <h1 i18n>User List Page</h1>
        </mat-toolbar>
        <router-outlet></router-outlet>
      </mat-sidenav-content>
    </mat-sidenav-container>
  `,
})
export class AppComponent implements OnInit {
  title = 'groweriq-signup';

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    console.log(this.activatedRoute.snapshot.data)
  }
}
