import {
  Directive,
  HostListener,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ElementRef
} from '@angular/core';

import {
  NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl, AbstractControl
} from '@angular/forms';

@Directive({
  selector: '[appPhoneMask]',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: PhoneMaskDirective,
    multi: true
  }]
})
export class PhoneMaskDirective implements ControlValueAccessor, OnChanges {

  onTouched: any;
  onChange: any;
  el: ElementRef;

  @Input() appPhoneMask: string;
  @Output() unmaskedValue = new EventEmitter();

  constructor(el: ElementRef) {
    this.el = el;
  }

  writeValue(value: any): void {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.el.nativeElement.value = '';
    this.el.nativeElement.dispatchEvent(new Event('blur'));
    this.unmaskedValue.emit('');
  }

  @HostListener('keyup', ['$event'])
  onKeyup($event: any) {
    const maxValueSize = this.getMaxValueSize();
    const numberValue = this.getNumericValue($event.target.value);

    const currentNumber = Object.values(numberValue);
    $event.target.value = Object.values(this.appPhoneMask).reduce((acc, current, idx) => {
      if (!currentNumber.length) { return acc; }

      if (isNaN(parseInt(current, 10))) {
        return acc + current;
      } else {
        return acc + currentNumber.shift();
      }
    }, '');

    if (numberValue.length === maxValueSize) {
      this.unmaskedValue.emit(numberValue);
    }

    return $event.target.value;
  }

  @HostListener('blur', ['$event'])
  onBlur($event: any) {
    // codigo
    if ($event.target.value.length === this.appPhoneMask.length) {
      return;
    }

    if (this.onChange) {
      this.onChange('');
    }
    $event.target.value = '';
    this.unmaskedValue.emit('');
  }

  private getNumericValue(targetValue) {
    const maxValueSize = this.getMaxValueSize();
    const currentValue = targetValue.replace(/\D/g, '');
    return (currentValue.length > maxValueSize) ? currentValue.slice(0, maxValueSize) : currentValue;
  }

  private getMaxValueSize() {
    return this.appPhoneMask.replace(/\D/g, '').length;
  }

}
