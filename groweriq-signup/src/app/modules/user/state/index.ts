import {UserState} from "./user.reducer";
import {createFeatureSelector, createSelector} from "@ngrx/store";

export interface State {
  users: UserState;
}

const getUserFeatureState = createFeatureSelector<UserState>('users');

export const getUserDetail = createSelector(
  getUserFeatureState,
  state => state.userDetail
)
