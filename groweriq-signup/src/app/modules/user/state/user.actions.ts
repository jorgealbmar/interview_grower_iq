import { createAction, props } from '@ngrx/store';
import {GrowerUser, User, WarehouseUser} from "../user.models";

export const setUserDetail = createAction('[User List] Set user detail', props<{ userDetail: GrowerUser | WarehouseUser }>())
