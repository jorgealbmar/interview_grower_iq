import {GrowerUser, User, WarehouseUser} from "../user.models";
import {createReducer, on} from "@ngrx/store";
import {setUserDetail} from "./user.actions";

export interface UserState {
  userDetail: GrowerUser | WarehouseUser
}

const initialState: UserState = {
  userDetail: null
}

export const userReducer = createReducer<UserState>(
  initialState,
  on(setUserDetail, (state, action): UserState => {
    return {
      ...state,
      userDetail: action.userDetail
    }
  })
)
