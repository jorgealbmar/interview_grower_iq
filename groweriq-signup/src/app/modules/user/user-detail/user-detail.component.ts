import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {UserState} from "../state/user.reducer";
import {GrowerUser, User, WarehouseUser} from "../user.models";
import {Observable} from "rxjs";
import {getUserDetail} from "../state";

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  userDetail$: Observable<GrowerUser | WarehouseUser>;

  constructor(private store: Store<UserState>) { }

  ngOnInit() {
    this.userDetail$ = this.store.select(getUserDetail);
  }

}
