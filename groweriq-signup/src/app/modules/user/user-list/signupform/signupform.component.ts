import {Component, Input} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserState} from "../../state/user.reducer";
import {Store} from "@ngrx/store";
import {setUserDetail} from "../../state/user.actions";
import {Router} from "@angular/router";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-signupform',
  templateUrl: './signupform.component.html',
  styleUrls: ['./signupform.component.scss']
})
export class SignupformComponent {
  signupGrowerUserForm: FormGroup;
  signupWarehouseUserForm: FormGroup;

  @Input()
  public userType: string;

  constructor(
    private fb: FormBuilder,
    private store: Store<UserState>,
    private router: Router,
    public dialogRef: MatDialogRef<SignupformComponent>
  ) {
    this.signupGrowerUserForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      address: ['', [Validators.required, Validators.minLength(3)]],
      phone_prefix: '+1',
      phone_number: '',
      gender: ['', [Validators.required]],
      batches_handled: ['', [Validators.required]],
      yield_acquired: [undefined, [Validators.required, Validators.min(0)]],
      greenhouse_locations: [[], [Validators.required]],
    });
    this.signupWarehouseUserForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      address: ['', [Validators.required, Validators.minLength(3)]],
      phone_prefix: '+1',
      phone_number: '',
      gender: ['', [Validators.required]],
      years_of_experience: [undefined, [Validators.required, Validators.min(0)]],
      educational_qualification: ['', [Validators.required]],
      inventory_management_certification: false
    });
  }

  handlePhoneNumber(value) {
    if (this.currentForm) {
      this.currentForm.get('phone_number').setValue(value);
    }
  }

  saveUser() {
    const { phone_prefix, phone_number, ...newUser} = this.currentForm.getRawValue();
    this.store.dispatch(setUserDetail({ userDetail: { ...newUser, phone_number: phone_prefix+phone_number, type: this.userType } }))
    this.dialogRef.close();
    this.router.navigate(['users/detail']);
    // this.userService.create(this.currentForm.getRawValue());
  }

  get currentForm(): FormGroup | undefined {
    if (!this.userType) {
      return;
    }
    return this.userType === 'grower' ? this.signupGrowerUserForm : this.signupWarehouseUserForm;
  }

  get mask() {
    return this.phonePrefix && this.phonePrefix.value === '+1' ? '(999) 999-9999' : '(99) 99999-9999';
  }

  get name() {
    return this.currentForm && this.currentForm.get('name');
  }

  get address() {
    return this.currentForm && this.currentForm.get('name');
  }

  get phonePrefix() {
    return this.currentForm && this.currentForm.get('phone_prefix');
  }

  get phoneNumber() {
    return this.currentForm && this.currentForm.get('phone_number');
  }

  get gender() {
    return this.currentForm && this.currentForm.get('gender');
  }

  get batchesHandled() {
    return this.currentForm && this.currentForm.get('batches_handled');
  }

  get yieldAcquired() {
    return this.currentForm && this.currentForm.get('yield_acquired');
  }

  get greenhouseLocations() {
    return this.currentForm && this.currentForm.get('greenhouse_locations');
  }

  get yearsOfExperience() {
    return this.currentForm && this.currentForm.get('years_of_experience');
  }

  get educationalQualification() {
    return this.currentForm && this.currentForm.get('educational_qualification');
  }

  get inventoryManagementCertification() {
    return this.currentForm && this.currentForm.get('inventory_management_certification');
  }

}
