import {AfterViewInit, Component} from '@angular/core';
import {Observable} from 'rxjs';
import {UserService} from '../../../services/user.service';
import {MatDialog} from '@angular/material/dialog';
import {SignupformComponent} from './signupform/signupform.component';
import {User} from "../user.models";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements AfterViewInit {

  userData$: Observable<Partial<User>[]>;
  visibleColumns = ['name', 'phone_number', 'type'];

  constructor(private userService: UserService, public dialog: MatDialog) { }

  ngAfterViewInit() {
    this.userData$ = this.userService.findAll();
  }

  openSignupForm() {
    this.dialog.open(SignupformComponent, {
      width: '500px'
    });
  }

}
