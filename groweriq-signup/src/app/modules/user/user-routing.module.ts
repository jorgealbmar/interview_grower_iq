import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserListComponent} from './user-list/user-list.component';
import {UserDetailComponent} from './user-detail/user-detail.component';

const routes: Routes = [
  {
    path: '',
    data: { name: 'User List' },
    component: UserListComponent
  },
  {
    path: 'detail',
    data: { name: 'User Detail' },
    component: UserDetailComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
