export interface User {
  name: string;
  address: string;
  phone_number: string;
  gender: string;
  type: 'grower' | 'warehouse';
}

export interface GrowerUser extends User {
  type: 'grower';
  batches_handled: number;
  yield_acquired: number;
  greenhouse_locations: string[];
}

export interface WarehouseUser extends User {
  type: 'warehouse';
  years_of_experience: number;
  educational_qualification: string;
  inventory_management_certification: boolean;
}
