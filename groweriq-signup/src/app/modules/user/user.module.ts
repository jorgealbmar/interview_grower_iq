import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserRoutingModule} from './user-routing.module';
import {UserListComponent} from './user-list/user-list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTableModule, MatToolbarModule, MatSidenavModule, MatIconModule, MatListModule} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import {PhoneMaskDirective} from '../../directives/phone-mask.directive';
import {SignupformComponent} from './user-list/signupform/signupform.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDialogModule} from '@angular/material/dialog';
import { LayoutModule } from '@angular/cdk/layout';
import { UserDetailComponent } from './user-detail/user-detail.component';
import {StoreModule} from "@ngrx/store";
import {userReducer} from "./state/user.reducer";

@NgModule({
  imports: [
    StoreModule.forFeature('users', userReducer),
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    HttpClientModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCheckboxModule,
    MatDialogModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  declarations: [
    UserListComponent,
    SignupformComponent,
    UserListComponent,
    PhoneMaskDirective,
    UserDetailComponent,
  ],
  providers: [HttpClientModule],
  entryComponents: [
    SignupformComponent
  ]
})
export class UserModule { }
