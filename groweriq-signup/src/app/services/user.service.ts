import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GrowerUser, User, WarehouseUser} from "../modules/user/user.models";

const BASE_URL = 'http://5ccc5842f47db800140110d0.mockapi.io/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<Partial<User>[]> {
    return this.http.get<Partial<User>[]>(BASE_URL);
  }

  create(user: GrowerUser | WarehouseUser) {
    return this.http.post(BASE_URL, user);
  }

}
